package org.fenrir.kraft.prototype;

import java.net.URL;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.kraft.prototype.ui.Bridge;

/**
 * TODO v1.0 Documentaci�n
 * @author Antonio Archilla Nava
 * @version v0.1.20140923
 */
public class MainProgram extends Application
{
	private final Logger log = LoggerFactory.getLogger(MainProgram.class);
	
	@Override
    public void start(Stage primaryStage) 
    {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() 
        {
            @Override
            public void uncaughtException(Thread t, Throwable e) 
            {
                log.error("ERROR: {}", e.getMessage(), e);
            }
        });
        
        // Creaci�n de la vista de renderizado y el motor de ejecuci�n de contenido web
        WebView browserView = new WebView();
        final WebEngine webEngine = browserView.getEngine();          
        webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() 
        {
            @Override
            public void changed(ObservableValue<? extends Worker.State> p, Worker.State oldState, Worker.State newState) 
            {
                if (newState==Worker.State.SUCCEEDED){
                	log.debug("Configurando Bridge java");
                	
                    JSObject jsobj = (JSObject)webEngine.executeScript("window");
                    jsobj.setMember("bridge", new Bridge());
                }
            }
        });
        
        webEngine.setOnAlert(new EventHandler<WebEvent<String>>() 
		{
			@Override
			public void handle(WebEvent<String> event) 
			{
				log.info("ALERT: {}", event.getData());
			}
		});
        // Se carga la p�gina HTML inicial de la aplicaci�n
        URL url = getClass().getResource("/org/fenrir/kraft/prototype/ui/html/main.html");
    	webEngine.load(url.toString());
               
        StackPane rootPanel = new StackPane();
        rootPanel.getChildren().add(browserView);        
        Scene scene = new Scene(rootPanel, 800, 600);
        primaryStage.setTitle("Kraft v0.1");
        primaryStage.setScene(scene);
        // Se maximizan las medidas de la ventana
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getBounds();
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        // Se hace visible la ventana
        primaryStage.show();
    }

    /**
     * El m�todo main() es ignorado en una aplicaci� JavaFX correctamente desplegada.
     * Sirve s�lo como fallback en caso de que la aplicaci�n no pueda ser lanzada a trav�s
     * de los artifacts de despliegue por un soporte limitado del IDE. Netbeans ignora este m�todo.
     * @param args {@link String}[]: Argumentos de la linea de comenado
     */
    public static void main(String... args) 
    {                
        launch(args);
    }
}
