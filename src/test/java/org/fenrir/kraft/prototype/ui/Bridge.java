package org.fenrir.kraft.prototype.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentación
 * @author Antonio Archilla Nava
 * @version v0.1.20140923
 */
public class Bridge 
{
	private final Logger log = LoggerFactory.getLogger(Bridge.class);

	public Object serve(String action)
	{
		log.debug("Sirviendo accion ", action);
		return "Hola " + action;
	}
}
