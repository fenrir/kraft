package org.fenrir.kraft.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.MatchQueryBuilder.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.fenrir.kraft.test.util.OrderedTest;
import org.fenrir.kraft.test.util.TestRunner;

@RunWith(TestRunner.class)
public class ElasticSearchConceptTestCase 
{
	private static final String ES_CLUSTER_NAME = "embedded-cluster";
	private static final String ES_NODE_NAME = "embedded-node";
	private static final String ES_PATH_DATA = "data/index";
	
	private static final String INDEX_NAME = "search-index";

	private final Logger log = LoggerFactory.getLogger(ElasticSearchConceptTestCase.class);
	
	private static Node node;
	private static Client client;
	
	@BeforeClass
	public static void initializeTest()
	{
		FileUtils.deleteQuietly(new File(ES_PATH_DATA));
		
		ImmutableSettings.Builder settings = ImmutableSettings.settingsBuilder();
	    settings.put("node.name", ES_NODE_NAME);
	    settings.put("path.data", ES_PATH_DATA);
	    settings.put("http.enabled", false);
	    node = NodeBuilder.nodeBuilder()
	        .settings(settings)
	        .clusterName(ES_CLUSTER_NAME)
	        .data(true).local(true).node();

	    client = node.client();
	}
	
	@AfterClass
	public static void tearDownTest()
	{
		if(client!=null){
			client.close();
		}
		if(node!=null){
			node.close();
		}
	}
	
	@OrderedTest(order=1)
	public void createIndex()
	{
		CreateIndexRequest request = Requests.createIndexRequest(INDEX_NAME);
		client.admin().indices().create(request).actionGet();
		
		IndicesExistsResponse response = client.admin().indices().exists(new IndicesExistsRequest(INDEX_NAME)).actionGet(); 
		Assert.assertTrue(response.isExists());
	}
	
	@OrderedTest(order=2)
	public void createMappings() throws IOException
	{
		PutMappingResponse response = client.admin().indices()
				.preparePutMapping(INDEX_NAME)
				.setType("mail")
				.setSource("{ \"mail\":{ \"properties\":{ \"summary\":{ \"type\":\"string\" }, \"body\":{ \"type\":\"string\" }, \"file\":{ \"type\":\"attachment\", \"fields\":{ \"file\":{ \"type\":\"string\", \"term_vector\":\"with_positions_offsets\", \"store\":true }, \"title\":{\"store\":\"yes\"}, \"date\":{\"store\":\"yes\"}, \"keywords\":{\"store\":\"yes\"}, \"content_type\":{\"store\":\"yes\"}, \"content_length\":{\"store\":\"yes\"}, \"language\":{\"store\":\"yes\"} } } } } }")
				.execute().actionGet();
		
		Assert.assertTrue(response.isAcknowledged());
	}
	
	@OrderedTest(order=3)
	public void insertDocument()
	{
		IndexRequest indexRequest = Requests.indexRequest(INDEX_NAME)
		        .type("documento")
		        .source("{title:\"Documento 1\", content:\"Hola\"}");
		IndexResponse response = client.index(indexRequest).actionGet();
		Assert.assertNotNull(response.getId());
		log.debug("A�adido nuevo documento con id={}", response.getId());
		
		client.admin().indices().refresh(Requests.refreshRequest(INDEX_NAME)).actionGet();
	}
	
	@OrderedTest(order=4)
	public void searchDocument()
	{
		QueryBuilder queryStringBuilder = QueryBuilders.queryString("hola")
				.field("content");

		SearchRequestBuilder requestBuilder = client.prepareSearch(INDEX_NAME)
				.setTimeout(TimeValue.timeValueSeconds(5))
				.setTypes("documento")				
				.setQuery(queryStringBuilder);
		
		SearchResponse response = requestBuilder.execute().actionGet();
		
		Assert.assertTrue(response.getHits()!=null && response.getHits().getTotalHits()>0);
		log.debug("N�mero de resultados: {}", response.getHits().getTotalHits());
		for(SearchHit hit:response.getHits()){
			log.debug("Recuperado resultado de la b�squeda= {}", hit.getSourceAsString());
		}
	}
	
	@OrderedTest(order=5)
	public void insertAttachment() throws Exception
	{
		IndexRequest indexRequest = Requests.indexRequest(INDEX_NAME)
		        .type("mail")
		        .source("{summary: \"Mail de pruebas 1\", "
		        		+ "body: \"Cuerpo del mail de pruebas con las palabras king queen\", "
		        		+ "file:\"IkdvZCBTYXZlIHRoZSBRdWVlbiIgKGFsdGVybmF0aXZlbHkgIkdvZCBTYXZlIHRoZSBLaW5nIg==\"}");
		IndexResponse response = client.index(indexRequest).actionGet();
		Assert.assertNotNull(response.getId());
		log.debug("A�adido nuevo documento con id={}", response.getId());
		
		client.admin().indices().refresh(Requests.refreshRequest(INDEX_NAME)).actionGet();
	}
	
	@OrderedTest(order=6)
	public void searchAttachment() throws Exception
	{
		SearchRequestBuilder requestBuilder = client.prepareSearch(INDEX_NAME)
				.setTimeout(TimeValue.timeValueSeconds(5))
				.setTypes("mail")				
				.setQuery(QueryBuilders.boolQuery()
						.must(QueryBuilders.termQuery("file", "king"))
						.must(QueryBuilders.termQuery("body", "king")));
		
		SearchResponse response = requestBuilder.execute().actionGet();
		
		log.debug("Respuesta recibida: {}", response.toString());
		Assert.assertTrue(response.getHits()!=null && response.getHits().getTotalHits()>0);
		log.debug("N�mero de resultados: {}", response.getHits().getTotalHits());
	}
}





	
//	@OrderedTest(order=2)
//	public void createMappings() throws IOException
//	{
//		XContentBuilder mapping = XContentFactory.jsonBuilder()
//				.startObject()
//					.startObject("mail")
//						.startObject("properties")
//							.startObject("summary")
//								.field("type", "string")                                
//                        	.endObject()
//                        	.startObject("body")
//                        		.field("type", "string")
//                    		.endObject()
//                    		.startObject("attachment")
//                        		.field("type", "attachment")
//                    			.startObject("fields")
//                    				.startObject("file")
//                    					.field("index", "yes")
//                    					.field("term_vector", "with_positions_offsets")
//                    				.endObject()
//                    				.startObject("title")
//                    					.field("store", "yes")
//                    				.endObject()
//                    				.startObject("date")
//                    					.field("store", "yes")
//                    				.endObject()
//                    				.startObject("keywords")
//                    					.field("store", "yes")
//                    				.endObject()
//                    				.startObject("content_type")
//                    					.field("store", "yes")
//                    				.endObject()
//                    				.startObject("content_length")
//                    					.field("store", "yes")
//                    				.endObject()
//                    			.endObject()
//                			.endObject()
//        				.endObject()
//    				.endObject();
//		
//		System.out.println("mapping: " + mapping.string());
//
//		PutMappingResponse response = client.admin().indices()
//				.preparePutMapping(INDEX_NAME)
//				.setType("mail")
//				.setSource(mapping)
//				.execute().actionGet();
//		
//		Assert.assertTrue(response.isAcknowledged());
//	}
	
//	@OrderedTest(order=5)
//	public void insertAttachment() throws Exception
//	{
//		// pdf en base64
//		int BUFFER_SIZE = 4096;
//		byte[] buffer = new byte[BUFFER_SIZE];
//		InputStream input = new FileInputStream("test/data/elasticsearch/2013-Scrum-Guide.pdf");
//		ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_SIZE);
//		OutputStream output = new Base64OutputStream(baos);
//		StringBuilder attachmentBuilder = new StringBuilder();
//		int n;
//		while((n = input.read(buffer, 0, BUFFER_SIZE)) >= 0){
//		    output.write(buffer, 0, n);
//		    attachmentBuilder.append(baos.toString());
//		}
//		input.close();
//		output.close();
//		
//		IndexRequest indexRequest = Requests.indexRequest(INDEX_NAME)
//		        .type("mail")
//		        .source("{summary:\"Correo 1\", "
//		        		+ "body:\"Esto es el cuerpo del correo 1\", "
//		        		+ "attachment:{_content_type: \"application/pdf\", "
//		        			+ "_name: \"resource/name/of/my.pdf\", "
//		        			+ "_content: \"" + attachmentBuilder.toString() + "\"}"
//    					+ "}");
//		IndexResponse response = client.index(indexRequest).actionGet();
//		Assert.assertNotNull(response.getId());
//		log.debug("A�adido nuevo documento con id={}", response.getId());
//		
//		client.admin().indices().refresh(Requests.refreshRequest(INDEX_NAME)).actionGet();
//		
//		GetResponse r = client.prepareGet(INDEX_NAME, "mail", response.getId())
//		        .execute()
//		        .actionGet();
//		
//		log.info("ATT: " + r.getSourceAsString());
//	}
	
//	@OrderedTest(order=6)
//	public void searchAttachment() throws Exception
//	{
//		QueryBuilder queryStringBuilder = QueryBuilders.queryString("scrum")
//				.field("file");
//		
//		SearchRequestBuilder requestBuilder = client.prepareSearch(INDEX_NAME)
//				.setTimeout(TimeValue.timeValueSeconds(5))
//				.setTypes("mail")				
//				.setQuery(queryStringBuilder)
//				.addHighlightedField("file");
//		
//		SearchResponse response = requestBuilder.execute().actionGet();
//		
//		Assert.assertTrue(response.getHits()!=null && response.getHits().getTotalHits()>0);
//		log.debug("N�mero de resultados: {}", response.getHits().getTotalHits());
//		for(SearchHit hit:response.getHits()){
//			log.debug("Recuperado resultado de la b�squeda= {}", hit.getSourceAsString());
//		}
//	}
