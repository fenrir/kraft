package org.fenrir.kraft.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import weka.classifiers.bayes.DMNBtext;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ArffSaver;
import weka.core.converters.TextDirectoryLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class WekaTestCase 
{
	private Instances instances;
	
	@BeforeTest
	@Parameters({"wekaTestTrainingDataFolder", "wekaTestModelFile"})
	public void setUp(@Optional("test/data/weka/training") String trainingFolder,
			@Optional("test/data/weka/modelFile.arff") String modelFile) throws Exception
	{
		if(new File(modelFile).exists()){
			FileUtils.forceDelete(new File(modelFile));
		}
		
		createModel(trainingFolder, modelFile);
		loadModel(modelFile);
	}
	
	@AfterTest
	@Parameters("wekaTestModelFile")
	public void tearDown(@Optional("test/data/weka/modelFile.arff") String modelFile)
	{
		saveModel(modelFile);
	}

	@Test(dataProvider="classificationDataProvider")
	public void classifyTest(String message, String expectedClass) throws Exception
	{	
		DMNBtext classifier = new DMNBtext();
		
		StringToWordVector filter = new StringToWordVector();
		filter.setInputFormat(instances);
		Instances filteredData  = Filter.useFilter(instances, filter);
		classifier.buildClassifier(filteredData);
		
		Instance instance = createInstance(message, instances.stringFreeStructure());
		
		filter.input(instance);
		Instance filteredInstance = filter.output();
		double predicted = classifier.classifyInstance(filteredInstance);
		String predictedClass = instances.classAttribute().value((int) predicted);
		// Log verbosity 7 -> INFO
		Reporter.log("Predicción de clase: " + predictedClass, 7, true);
		Assert.assertEquals(predictedClass, expectedClass);
	}
	
	private void createModel(String trainingFolder, String modelFile) throws Exception
	{
		TextDirectoryLoader loader = new TextDirectoryLoader();
		loader.setDirectory(new File(trainingFolder));
		FileUtils.writeStringToFile(new File(modelFile), loader.getDataSet().toString());
	}
	
	private void loadModel(String fileName) throws Exception
	{
		try{
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			ArffReader arff = new ArffReader(reader);
			instances = arff.getData();
			instances.setClassIndex(instances.numAttributes()-1);
			reader.close();
		}
		catch (IOException e) {
			// Log verbosity 3 -> ERROR
			Reporter.log("No se ha podido cargar el modelo " + fileName + ": " + e.getMessage(), 3, true);
		}
	}
	
	private void saveModel(String filename)
	{
		try{
			ArffSaver saver = new ArffSaver();
  		  	saver.setInstances(instances);
  		  	saver.setFile(new File(filename));
  		  	saver.writeBatch();
			
			// Log verbosity 7 -> INFO
			Reporter.log("Modelo guardado: " + filename, 7, true);
		}
		catch(IOException e){
			// Log verbosity 3 -> ERROR
			Reporter.log("No se ha podido guardar el modelo " + filename + ": " + e.getMessage(), 3, true);
		}
	}
	
	private Instance createInstance(String text, Instances data) 
	{
	    Instance instance = new Instance(2);
	    Attribute messageAtt = data.attribute("text");
	    instance.setValue(messageAtt, messageAtt.addStringValue(text));

	    instance.setDataset(data);
	    
	    return instance;
	}
	
	@DataProvider(name="classificationDataProvider")
	public Object[][] getClassificationData()
	{
		return new Object[][]{
				// issueReport 
				{"from: aaa@bbb.com subject: Error 9 date: 07/10/2014 02:00:01 content: 	Reportado por: fffff 	Descripción: sdfksjgdfks vsdiut9uwfjnlksd isdhofjsodf oisdfousoudgfosj osidhfoshodfsiudguf", "issueReports"},
				{"from: ccc@ddd.com subject: random content date: 22/11/2014 22:20:01 content: sjdfkjs ksjdfkjsf kjhdfsyfu58guf oihofihwoief", "altres"},
				{"from: aaa@bbb.com subject: Error 100 date: 21/01/2013 02:00:01 content: 	Reportado por: asdf 	Descripción: sjdfkjs ksjdfkjsf kjhdfsyfu58guf oihofihwoief", "issueReports"},
				{"from: aaa@bbb.com subject: XSD Error date: 15/08/2013 13:54:24 content: fhgiufi eufgeoiuvbr iurgieugfeibv", "spam"}
		};
	}
}
