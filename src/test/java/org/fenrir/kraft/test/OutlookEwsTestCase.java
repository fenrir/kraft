package org.fenrir.kraft.test;

import microsoft.exchange.webservices.data.AutodiscoverLocalException;
import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.ExchangeVersion;
import microsoft.exchange.webservices.data.FindFoldersResults;
import microsoft.exchange.webservices.data.Folder;
import microsoft.exchange.webservices.data.FolderTraversal;
import microsoft.exchange.webservices.data.FolderView;
import microsoft.exchange.webservices.data.IAutodiscoverRedirectionUrl;
import microsoft.exchange.webservices.data.WebCredentials;
import microsoft.exchange.webservices.data.WellKnownFolderName;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class OutlookEwsTestCase 
{
	private ExchangeService service;
	
	@BeforeTest
	public void setUp() throws Exception
	{
		service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
		ExchangeCredentials credentials = new WebCredentials("aaa@bbb.com", "password");
		service.setCredentials(credentials);
		service.autodiscoverUrl("aaa@bbb.com", new IAutodiscoverRedirectionUrl() 
		{
			@Override
			public boolean autodiscoverRedirectionUrlValidationCallback(String redirectionUrl) throws AutodiscoverLocalException 
			{
				return true;
			}
		});
	}
	
	@Test
	public void listFolders() throws Exception
	{
		FolderView view = new FolderView(50);
		view.setTraversal(FolderTraversal.Deep);
		FindFoldersResults folders = service.findFolders(WellKnownFolderName.Root, view);
		
		for(Folder folder:folders){
			System.out.println("FOLDER: " + folder.getId() + " / " + folder.getDisplayName());
		}
	}
}
