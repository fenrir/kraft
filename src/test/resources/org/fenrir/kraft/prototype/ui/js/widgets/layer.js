define(['jquery', 
		'jquery.blockUI.min',	
		'backbone'],
function($, blockUI, Backbone){
	
	var LayerModel = Backbone.Model.extend(
	{
		defaults:{
			domId: undefined,
			visible: false,
			text: undefined			
		},

		initialize: function(){ }	
	});
	
	var Layer = Backbone.View.extend(
	{	
		model: new LayerModel(),
	
		initialize: function(options)	
		{			
			this.model.set("domId", options.domId); 			
			this.model.set("text", options.text);	

			if(options.domId!==undefined){
				this.$el = $(options.domId);
			}
		},
		
		render: function() 
		{	
			if(this.model.get("domId")===undefined){		
//				var target = this.model.get("visible") ? $.blockUI : $.unblockUI;
				if(this.model.get("visible")){
					$.blockUI({ 
						message: this.model.get("text") 
					}); 
				}
				else{
					$.unblockUI({ fadeOut: 200 }); 
				}
			}
			else{
//				var target = this.model.get("visible") ? this.$el.block : this.$el.unblock;
				if(this.model.get("visible")){
					this.$el.block({ 
						message: this.model.get("text") 
					}); 
				}
				else{
					this.$el.unblock({ fadeOut: 200 }); 
				}
			}
		
			return this;
		},
			
		setVisible: function(visible)
		{
			this.model.set("visible", visible);
			this.render();
		},
		
		isVisible: function()
		{
			return this.model.get("visible");
		}
	});
	
	return Layer;
});