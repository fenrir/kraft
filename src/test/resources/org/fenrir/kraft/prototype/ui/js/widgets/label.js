define(['jquery', 
		'jquery.qtip.min',	
		'backbone',
		'cssUtils'],
function($, qtip, Backbone, cssUtils){
	
	var LabelModel = Backbone.Model.extend(
	{
		defaults:{
			domId: undefined,
			text: undefined,
			tooltip: undefined
		},

		initialize: function(){ }	
	});
	
	var Label = Backbone.View.extend(
	{	
		model: new LabelModel(),
	
		initialize: function(options)	
		{			
			this.model.set("domId", options.domId); 
			this.model.set("text", options.text); 
			this.model.set("tooltip", options.tooltip); 
			
			this.$el = $(options.domId);
	
			cssUtils.loadCss('../css/jquery.qtip.min.css');
			
			this.render();
		},
		
		render: function() 
		{			
			this.$el.text(this.model.get("text"));
			this.$el.qtip({ 
				style: {
					classes: 'qtip-youtube',					
				},
				position: {
					my: 'top center',
					at: 'bottom center',
					target: this.$el
				},
			    content: {
			        text: this.model.get("tooltip")
			    }
			});			
			
			return this;
		}
	});
	
	return Label;
});