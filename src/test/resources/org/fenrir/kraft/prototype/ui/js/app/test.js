define(['jquery'],
function($){
	return{
        writeContents: function(elementId, str)
        {
			$("#"+elementId).text(str);
		}
    }
});