requirejs.config({
    // Por defecto carga cualquier m�dulo de js/lib
    baseUrl: '../js/lib',
    /* Excepto si la ID del m�dulo empieza por "app",
     * que lo har� desde js/app. Los paths son relativos
     * a baseUrl i nunca incluyen ".js" como extension
     */
    paths: {
        app: '../app',
		widgets: '../widgets',		
        jquery: 'jquery-1.7.2.min',
        backbone: 'backbone-min',
        underscore: 'underscore-min'
    }
});

requirejs(['jquery', 'app/test', 'widgets/label', 'widgets/layer'],
function($, test, Label, Layer)
{
	//Test sobreescritura jQuery
	$.extend({
		get : function(url){ test.writeContents("contentArea", bridge.serve(url)); }
	});		
	
	new Label({
		domId: "#contentArea",		
		text: "Etiqueta de prueba",
		tooltip: "Tooltip de prueba"
	});
	
	var mainLayer = new Layer({
//		domId: "#internalPanel",
		text: "En proceso..."
	});
	
	$('#submit').on('click', function(e) {
		mainLayer.setVisible(true);
	});	
	
	$(document).keyup(function(e) {	  
		if (e.keyCode == 27) { mainLayer.setVisible(false); }   // esc
	});
});
