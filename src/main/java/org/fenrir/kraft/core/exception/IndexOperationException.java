package org.fenrir.kraft.core.exception;

@SuppressWarnings("serial")
public class IndexOperationException extends Exception 
{
    public IndexOperationException() 
    {
        super();	
    }

    public IndexOperationException(String message, Throwable cause) 
    {
        super(message, cause);
    }

    public IndexOperationException(String message) 
    {
        super(message);	
    }

    public IndexOperationException(Throwable cause) 
    {
        super(cause);	
    }
}