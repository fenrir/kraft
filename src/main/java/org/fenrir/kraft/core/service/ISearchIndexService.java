package org.fenrir.kraft.core.service;

import org.fenrir.kraft.core.exception.IndexOperationException;

public interface ISearchIndexService 
{
	public void setUpConnection();
	public void tearDownConnection();
	
	public String getIndexFolderPath();
	
	public void createIndex() throws IndexOperationException;
	public void createIndex(String indexName) throws IndexOperationException;
	
	public void indexObject(String documentType, Object data) throws IndexOperationException;
	public void indexObject(String indexName, String documentType, Object data) throws IndexOperationException;
}
