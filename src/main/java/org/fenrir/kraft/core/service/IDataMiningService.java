package org.fenrir.kraft.core.service;

import org.fenrir.kraft.spi.exception.BusinessException;

public interface IDataMiningService 
{
	public void initialize() throws BusinessException;
}
