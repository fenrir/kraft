package org.fenrir.kraft.core.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import weka.classifiers.bayes.DMNBtext;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ArffLoader.ArffReader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.kraft.core.service.IDataMiningService;
import org.fenrir.kraft.spi.exception.BusinessException;

public class WekaDataMiningServiceImpl implements IDataMiningService 
{
	private final Logger log = LoggerFactory.getLogger(WekaDataMiningServiceImpl.class);
	
	private Instances instances;
	private DMNBtext classifier;
	
	@Override
	public void initialize() throws BusinessException
	{
//		instances = loadModel(fileName);
		
		classifier = new DMNBtext();
		
		try{
			StringToWordVector filter = new StringToWordVector();
			filter.setInputFormat(instances);
			Instances filteredData  = Filter.useFilter(instances, filter);
			classifier.buildClassifier(filteredData);
		}
		catch(Exception e){
			throw new BusinessException("Error inicializando el clasificador: " + e.getMessage(), e);
		}
	}
	
	private Instances loadModel(String filename) throws IOException
	{
		try{
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			ArffReader arff = new ArffReader(reader);
			Instances instances = arff.getData();
			instances.setClassIndex(instances.numAttributes()-1);
			reader.close();
			
			return instances;
		}
		catch(IOException e){
			log.error("No se ha podido cargar el modelo {}: {}", new Object[]{filename, e.getMessage(), e});
			throw e;
		}
	}
	
	private void saveModel(Instances instances, String filename) throws IOException
	{
		try{
			ArffSaver saver = new ArffSaver();
  		  	saver.setInstances(instances);
  		  	saver.setFile(new File(filename));
  		  	saver.writeBatch();
			
			log.info("Modelo guardado: {}", filename);
		}
		catch(IOException e){
			log.error("No se ha podido guardar el modelo {}: {}", new Object[]{filename, e.getMessage(), e});
			throw e;
		}
	}
}
