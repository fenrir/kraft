package org.fenrir.kraft.core.service.impl;

import java.io.IOException;
import javax.inject.Inject;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.kraft.core.service.ISearchIndexService;
import org.fenrir.kraft.core.exception.IndexOperationException;

public class ElasticSearchIndexServiceImpl implements ISearchIndexService 
{
	private static final String ES_CLUSTER_NAME = "embedded-cluster";
	private static final String ES_NODE_NAME = "embedded-node";
	private static final String ES_INDEX_DIR = "index";
	
	private static final String DEFAULT_INDEX_NAME = "default-search-index";
	
	private final Logger log = LoggerFactory.getLogger(ElasticSearchIndexServiceImpl.class);
	
	private static Node node;
	private static Client client;
	
	@Inject 
	private IWorkspaceAdministrationService workspaceService;
	
	public void serWorkspaceService(IWorkspaceAdministrationService workspacesService)
	{
		this.workspaceService = workspacesService;
	}
	
	@Override
	public void setUpConnection()
	{
		ImmutableSettings.Builder settings = ImmutableSettings.settingsBuilder();
	    settings.put("node.name", ES_NODE_NAME);
	    settings.put("path.data", getIndexFolderPath());
	    settings.put("http.enabled", false);
	    node = NodeBuilder.nodeBuilder()
	        .settings(settings)
	        .clusterName(ES_CLUSTER_NAME)
	        .data(true).local(true).node();

	    client = node.client();
	}
	
	@Override
	public void tearDownConnection()
	{
		if(client!=null){
			client.close();
		}
		if(node!=null){
			node.close();
		}
	}
	
	@Override
	public String getIndexFolderPath()
	{
		return workspaceService.getCurrentWorkspaceFolder() + "/" + ES_INDEX_DIR;
	}
	
	@Override
	public void createIndex() throws IndexOperationException
	{
		createIndex(DEFAULT_INDEX_NAME);
	}
	
	@Override
	public void createIndex(String indexName) throws IndexOperationException
	{
		IndicesExistsResponse response = client.admin().indices().exists(new IndicesExistsRequest(indexName)).actionGet();
		// Si el �ndice ya existe, no se hace nada
		if(response.isExists()){
			return;
		}
		
		CreateIndexRequest request = Requests.createIndexRequest(indexName);
		client.admin().indices().create(request).actionGet();
		
		response = client.admin().indices().exists(new IndicesExistsRequest(indexName)).actionGet();
		if(response.isExists()){
			throw new IndexOperationException("El �ndice no se ha creado correctamente");
		}
	}

	@Override
	public void indexObject(String documentType, Object data) throws IndexOperationException
	{
		indexObject(DEFAULT_INDEX_NAME, documentType, data);
	}
	
	@Override
	public void indexObject(String indexName, String documentType, Object data) throws IndexOperationException
	{
		try{
			ObjectMapper mapper = new ObjectMapper();
			String jsonData = mapper.writeValueAsString(data);
			
			IndexRequest indexRequest = Requests.indexRequest(indexName)
			        .type(documentType)
			        .source(jsonData);
			IndexResponse response = client.index(indexRequest).actionGet();
			log.debug("A�adido nuevo documento con id={}", response.getId());
			
			// Importante refrescar despu�s de la inserci�n para que los cambios est�n disponibles immediatamente
			client.admin().indices().refresh(Requests.refreshRequest(indexName)).actionGet();
		}
		catch(IOException e){
			throw new IndexOperationException("Error transformando para su indexado: " + e.getMessage(), e);
		}
	}
}
