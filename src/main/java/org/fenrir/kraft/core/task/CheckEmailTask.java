package org.fenrir.kraft.core.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckEmailTask implements Runnable
{
	public static final String ID = "org.fenrir.kraft.core.task.checkEmailTask";
    
    private final Logger log = LoggerFactory.getLogger(CheckEmailTask.class);
    
    @Override
    public void run()
    {
        if(log.isDebugEnabled()){
            log.debug("Iniciada tarea de comprobación de correos");
        }
    }
}
