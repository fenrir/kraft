package org.fenrir.kraft.ews.dto.adapter;

import java.util.Date;
import microsoft.exchange.webservices.data.Item;
import microsoft.exchange.webservices.data.MessageBody;
import microsoft.exchange.webservices.data.ServiceLocalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.kraft.spi.dto.IItemDto;

public class ItemDtoAdapter implements IItemDto 
{
	private final Logger log = LoggerFactory.getLogger(ItemDtoAdapter.class);
	
	private Item item;
	
	public ItemDtoAdapter(Item item)
	{
		this.item = item;
	}

	@Override
	public String getSubject()
	{
		try{
			return item.getSubject();
		}
		catch(ServiceLocalException e){
			log.error("Error recuperando los datos del Item: {}", e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	@Override
	public String getBody()
	{
		try{
			return MessageBody.getStringFromMessageBody(item.getBody());
		}
		catch(Exception e){
			log.error("Error recuperando los datos del Item: {}", e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@Override
	public Date getReceivedDate()
	{
		try{
			return item.getDateTimeReceived();
		}
		catch(ServiceLocalException e){
			log.error("Error recuperando los datos del Item: {}", e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	@Override
	public boolean hasAttachments()
	{
		try{
			return item.getHasAttachments();
		}
		catch(ServiceLocalException e){
			log.error("Error recuperando los datos del Item: {}", e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
