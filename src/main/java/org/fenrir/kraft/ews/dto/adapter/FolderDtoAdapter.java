package org.fenrir.kraft.ews.dto.adapter;

import microsoft.exchange.webservices.data.Folder;
import microsoft.exchange.webservices.data.ServiceLocalException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.kraft.spi.dto.IFolderDto;

public class FolderDtoAdapter implements IFolderDto 
{
	private final Logger log = LoggerFactory.getLogger(FolderDtoAdapter.class);
	
	private Folder folder;
	
	public FolderDtoAdapter(Folder folder)
	{
		this.folder = folder;
	}
	
	@Override
	public String getId() 
	{
		return folder.getId().getUniqueId();
	}

	@Override
	public String getDisplayName() 
	{
		try{
			return folder.getDisplayName();
		}
		catch(ServiceLocalException e){
			log.error("Error recuperando los datos del Folder: {}", e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
