package org.fenrir.kraft.ews.service.impl;

import java.util.LinkedList;
import java.util.List;

import microsoft.exchange.webservices.data.AutodiscoverLocalException;
import microsoft.exchange.webservices.data.ConflictResolutionMode;
import microsoft.exchange.webservices.data.EmailMessage;
import microsoft.exchange.webservices.data.EmailMessageSchema;
import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.ExchangeVersion;
import microsoft.exchange.webservices.data.FindFoldersResults;
import microsoft.exchange.webservices.data.FindItemsResults;
import microsoft.exchange.webservices.data.Folder;
import microsoft.exchange.webservices.data.FolderId;
import microsoft.exchange.webservices.data.FolderTraversal;
import microsoft.exchange.webservices.data.FolderView;
import microsoft.exchange.webservices.data.IAutodiscoverRedirectionUrl;
import microsoft.exchange.webservices.data.Item;
import microsoft.exchange.webservices.data.ItemId;
import microsoft.exchange.webservices.data.ItemView;
import microsoft.exchange.webservices.data.LogicalOperator;
import microsoft.exchange.webservices.data.SearchFilter;
import microsoft.exchange.webservices.data.WebCredentials;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.kraft.ews.dto.adapter.FolderDtoAdapter;
import org.fenrir.kraft.ews.dto.adapter.ItemDtoAdapter;
import org.fenrir.kraft.spi.dto.IFolderDto;
import org.fenrir.kraft.spi.dto.IItemDto;
import org.fenrir.kraft.spi.dto.ResultSet;
import org.fenrir.kraft.spi.exception.BusinessException;
import org.fenrir.kraft.spi.service.IWebMailService;

public class Outlook365EwsServiceImpl implements IWebMailService 
{
	private final Logger log = LoggerFactory.getLogger(Outlook365EwsServiceImpl.class);
	
	private ExchangeService service;
	
	@Override
	public void connect(String user, String password) throws BusinessException
	{
		try{
			service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
			ExchangeCredentials credentials = new WebCredentials(user, password);
			service.setCredentials(credentials);
			service.autodiscoverUrl(user, new IAutodiscoverRedirectionUrl() 
			{
				@Override
				public boolean autodiscoverRedirectionUrlValidationCallback(String redirectionUrl) throws AutodiscoverLocalException 
				{
					return true;
				}
			});
		}
		catch(Exception e){
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	@Override
	public ResultSet<IFolderDto> listSubFolders(String folderId, int offset, int pageSize, boolean deepSearch) throws BusinessException
	{
		try{
			FolderView view = new FolderView(pageSize, offset);
			if(deepSearch){
				view.setTraversal(FolderTraversal.Deep);
			}
			FindFoldersResults folders = service.findFolders(new FolderId(folderId), view);
			List<IFolderDto> results = new LinkedList<IFolderDto>();
			for(Folder folder:folders){
				results.add(new FolderDtoAdapter(folder));
			}
			
			return new ResultSet<IFolderDto>(results, offset, pageSize, folders.getTotalCount());
		}
		catch(Exception e){
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	@Override
	public ResultSet<IItemDto> listFolderItems(String folderId, int offset, int pageSize) throws BusinessException
	{
		try{
			ItemView view = new ItemView(pageSize, offset);
			FindItemsResults<Item> items = service.findItems(new FolderId(folderId), view);
			List<IItemDto> results = new LinkedList<IItemDto>();
			for(Item item:items){
				results.add(new ItemDtoAdapter(item));
			}
			
			return new ResultSet<IItemDto>(results, offset, pageSize, items.getTotalCount());
		}
		catch(Exception e){
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	@Override
	public ResultSet<IItemDto> listFolderUnreadItems(String folderId, int offset, int pageSize) throws BusinessException
	{
		try{
			ItemView view = new ItemView(pageSize, offset);
			SearchFilter filter = new SearchFilter.SearchFilterCollection(LogicalOperator.And, new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false));
			FindItemsResults<Item> items = service.findItems(new FolderId(folderId), filter, view);
			List<IItemDto> results = new LinkedList<IItemDto>();
			for(Item item:items){
				results.add(new ItemDtoAdapter(item));
			}
			
			return new ResultSet<IItemDto>(results, offset, pageSize, items.getTotalCount());
		}
		catch(Exception e){
			throw new BusinessException(e.getMessage(), e);
		}
	}

	@Override
	public void moveItemToFolder(String itemId, String destinationFolderId) throws BusinessException
	{
		try{
			Item item = Item.bind(service, new ItemId(itemId));
			FolderId destinationFolder = new FolderId(destinationFolderId);
			// Se verifica que el directorio actual del item != del de destino. En caso de ser iguales, no se hace nada
			if(destinationFolder.equals(item.getParentFolderId())){
				log.info("El item {} ya se encuentra en el directorio de destino {}", itemId, destinationFolderId);
			}
			
			item.move(destinationFolder);
		}
		catch(Exception e){
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	@Override
	public void markItemAsReaded(String itemId) throws BusinessException
	{
		try{
			EmailMessage message = EmailMessage.bind(service, new ItemId(itemId));
			message.setIsRead(true);
			message.update(ConflictResolutionMode.AlwaysOverwrite);
		}
		catch(Exception e){
			throw new BusinessException(e.getMessage(), e);
		}
	}
}
