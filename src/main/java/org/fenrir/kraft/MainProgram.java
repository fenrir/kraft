package org.fenrir.kraft;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.kraft.web.WebServerApplication;
import org.fenrir.yggdrasil.cli.CliApplication;
import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

public class MainProgram 
{
	private static final String OPTION_MODE_CLI = "cli";
	private static final String OPTION_MODE_GUI = "gui";
	private static final String OPTION_MODE_WEB = "web";
	
    public static void main(String... args) throws Exception
    {                
        Logger log = LoggerFactory.getLogger(MainProgram.class);
        
        Options options = buildOptions();
		CommandLineParser parser = new BasicParser();
        CommandLine commandLine = buildCommandLine(args, options, parser);
        
        AbstractApplication application;
        if(commandLine.hasOption(OPTION_MODE_CLI) && CliApplication.checkCompatibility()){
        	application = new CliApplication();
        }
        else if(commandLine.hasOption(OPTION_MODE_GUI)){
        	// TODO UiApplication
        	application = new CliApplication();
        }
        else if(commandLine.hasOption(OPTION_MODE_WEB) && WebServerApplication.checkCompatibility()){
        	application = new WebServerApplication();
        }
        else if(false){
        	// TODO UiApplication
        	application = new CliApplication();
        }
        else{
        	application = new WebServerApplication();
        }

        boolean error = false;
        try{
            application.initialize();
        }
        catch(ApplicationException e){
            log.error("Error fatal inicializando la aplicaci�n: {}", e.getMessage(), e);
            // No se puede hacer un exit -1 porqu� se cierra la ventana de error
            error = true;
        }

        if(!error){
	        try{
	            application.run();
	        }
	        catch(ApplicationException e){
	            log.error("Error fatal ejecutando la aplicaci�n: {}", e.getMessage(), e);
	            // No se puede hacer un exit -1 porqu� se cierra la ventana de error
	        }
        }        
    }
    
    private static Options buildOptions() 
	{
        Option optionCliMode = OptionBuilder.withArgName(OPTION_MODE_CLI)
        		.withDescription("Mode de funcionament per linia de comandes")
                .hasArg(false)
                .isRequired(false)
                .withType(String.class)
                .create(OPTION_MODE_CLI);
        
        Option optionGuiMode = OptionBuilder.withArgName(OPTION_MODE_GUI)
        		.withDescription("Mode de funcionament per interf�cie gr�fica")
                .hasArg(false)
                .isRequired(false)
                .create(OPTION_MODE_GUI);
        
        Option optionRemoteMode = OptionBuilder.withArgName(OPTION_MODE_WEB)
        		.withDescription("Mode de funcionament remot")
                .hasArg(false)
                .isRequired(false)
                .create(OPTION_MODE_WEB);
        
        Options options = new Options();
        options.addOption(optionCliMode);
        options.addOption(optionGuiMode);
        options.addOption(optionRemoteMode);
        
        return options;
    }
	
	private static CommandLine buildCommandLine(String[] args, Options options, CommandLineParser parser) 
	{
        try{
            return parser.parse(options, args);
        }
        catch(ParseException e){
            System.err.println("Error interpretant la l�nia de comandes: " + e.getMessage());
            System.exit(1);
            // No es cridar� mai...
            return null;
        }
    }
}