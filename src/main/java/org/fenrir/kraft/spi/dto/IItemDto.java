package org.fenrir.kraft.spi.dto;

import java.util.Date;

public interface IItemDto 
{
	public String getSubject();
	public String getBody();
	public Date getReceivedDate();
	public boolean hasAttachments();
}
