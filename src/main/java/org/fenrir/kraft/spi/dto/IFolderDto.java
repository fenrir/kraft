package org.fenrir.kraft.spi.dto;

public interface IFolderDto 
{
	public String getId();
	public String getDisplayName();
}
