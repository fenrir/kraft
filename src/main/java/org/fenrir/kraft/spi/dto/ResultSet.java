package org.fenrir.kraft.spi.dto;

import java.util.List;

public class ResultSet<T> 
{
	private List<T> elements;
	private int offset;
	private int pageSize;
	private int totalCount;
	
	public ResultSet(List<T> elements, int offset, int pageSize, int totalCount)
	{
		this.elements = elements;
		this.offset = offset;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
	}
	
	public List<T> getElements()
	{
		return elements;
	}
	
	public int getOffset()
	{
		return offset;
	}
	
	public int getPageSize()
	{
		return pageSize;
	}
	
	public int getTotalCount()
	{
		return totalCount;
	}
	
	public boolean isMoreAvailable()
	{
		return offset+pageSize < totalCount;
	}
}
