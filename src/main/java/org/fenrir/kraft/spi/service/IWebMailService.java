package org.fenrir.kraft.spi.service;

import org.fenrir.kraft.spi.dto.IFolderDto;
import org.fenrir.kraft.spi.dto.IItemDto;
import org.fenrir.kraft.spi.dto.ResultSet;
import org.fenrir.kraft.spi.exception.BusinessException;

public interface IWebMailService 
{
	public void connect(String user, String password) throws BusinessException;
	
	public ResultSet<IFolderDto> listSubFolders(String folderId, int offset, int pageSize, boolean deepSearch) throws BusinessException;
	public ResultSet<IItemDto> listFolderItems(String folderId, int offset, int pageSize) throws BusinessException;
	public ResultSet<IItemDto> listFolderUnreadItems(String folderId, int offset, int pageSize) throws BusinessException;
	
	public void moveItemToFolder(String itemId, String destinationFolderId) throws BusinessException;
	public void markItemAsReaded(String itemId) throws BusinessException;
}
