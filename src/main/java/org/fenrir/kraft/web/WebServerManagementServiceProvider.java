package org.fenrir.kraft.web;

import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.extension.IUIManagementService;

/**
 * TODO Documentació
 * @author aarchilla
 * @version 0.2.20150222
 */
public class WebServerManagementServiceProvider implements IUIManagementService 
{
	@Override
	public void initialize(AbstractApplication application) 
	{
		WebServerManager.getInstance().initialize(application);
	}

	@Override
	public boolean isCompatible() 
	{
		return checkCompatibility();
	}
	
	public static boolean checkCompatibility()
	{
		return true;
	}

	@Override
	public void notifyPreUIOpenEvent() 
	{
		// Res a fer...
	}

	@Override
	public void displayErrorMessage(String message, Throwable error) 
	{
		WebServerManager.getInstance().displayErrorMessage(message, error);
	}

	@Override
	public void displayStandaloneErrorMessage(String message, Throwable error) 
	{
		WebServerManager.displayStandaloneErrorMessage(message, error);
	}

	@Override
	public void createUI() throws ApplicationException 
	{
		WebServerManager.getInstance().createServer();
	}

	@Override
	public void showUI() throws ApplicationException 
	{
		WebServerManager.getInstance().startServer();
	}
}
