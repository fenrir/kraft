package org.fenrir.kraft.web.filter;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;

public class ActiveWorkspaceCheckFilter implements Filter 
{
	private final Logger log = LoggerFactory.getLogger(ActiveWorkspaceCheckFilter.class);
	
	@Inject
	private IWorkspaceAdministrationService workspaceService;
	
	private boolean workspaceInitialized = false;
	
	public void setWorkspaceService(IWorkspaceAdministrationService workspaceService)
	{
		this.workspaceService = workspaceService;
	}
	
	@Override
	public void init(FilterConfig config) throws ServletException 
	{
		log.info("Activat filtre ActiveWorkspaceCheckFilter [{}]", this.toString());
		
		ApplicationContext.getInstance().injectMembers(this);
	}
	
	@Override
	public void destroy() 
	{
		// Nada a hacer...
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
	{
		String url = ((HttpServletRequest)request).getRequestURL().toString();
		
		if(url.matches(".*(?:/kraft/{0,1}|\\.html)")){
			if(!workspaceInitialized && !workspaceService.isWorkspaceLoaded()){
				((HttpServletRequest)request).getRequestDispatcher("/action/switchworkspace.html").forward(request, response);
				return;
			}
			else if(workspaceService.isWorkspaceLoaded()){
				workspaceInitialized = true;
			}
		}
		
		chain.doFilter(request, response);
	}
}
