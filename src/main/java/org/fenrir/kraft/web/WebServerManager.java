package org.fenrir.kraft.web;

import java.util.EnumSet;
import javax.servlet.DispatcherType;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.kraft.web.filter.ActiveWorkspaceCheckFilter;
import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO Documentació
 * @author aarchilla
 * @version 0.2.20150228
 */
public class WebServerManager 
{
	private static final Logger log = LoggerFactory.getLogger(WebServerManager.class);
	
	private static WebServerManager instance;
	
	private Server server;
	
	private WebServerManager()
	{
		
	}
	
	public static WebServerManager getInstance()
	{
		if(instance==null){
			instance = new WebServerManager();
		}
		
		return instance;
	}
	
	public void initialize(AbstractApplication application)
    {
		
    }
	
	/**
	 * TODO Parametrizar configuración del servidor 
	 */
	public void createServer()
	{
		server = new Server(8080);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/kraft");
        context.setResourceBase(getClass().getClassLoader().getResource("org/fenrir/kraft/webapp").toExternalForm());
        context.setWelcomeFiles(new String[]{ "index.html" });
        /* Servlets */
        // Static content
        context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
        context.setInitParameter("org.eclipse.jetty.servlet.Default.welcomeServlets", "true");
        context.addServlet(DefaultServlet.class, "/*");
        /* Filters */
        context.addFilter(ActiveWorkspaceCheckFilter.class, "/*",
                EnumSet.of(DispatcherType.REQUEST));
		
		HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[] { context, new DefaultHandler() });
        server.setHandler(handlers);
	}
	
	public void startServer() throws ApplicationException
	{
		try{
			server.start();
			server.join();
		}
		catch(Exception e){
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	public void displayErrorMessage(final String message, final Throwable error)
	{
		log.error("{}: {}", new Object[]{message, error.getMessage(), error});
	}
	
	public static void displayStandaloneErrorMessage(final String message, final Throwable error)
	{
		log.error("{}: {}", new Object[]{message, error.getMessage(), error});
	}
}
