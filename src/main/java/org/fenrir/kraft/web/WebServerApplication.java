package org.fenrir.kraft.web;

import org.fenrir.yggdrasil.cli.CliConstants;
import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.2.20150222
 */
public class WebServerApplication extends AbstractApplication 
{
	@Override
	public void initialize() throws ApplicationException
	{
		// Embedded CLI application profile
		ApplicationContext.getInstance().addProfile(CliConstants.PROFILE_EMBEDDED_CLI_PROFILE);
		
		initialize(null, new WebServerManagementServiceProvider());
	}
	
	public static boolean checkCompatibility()
	{
		return WebServerManagementServiceProvider.checkCompatibility();
	}
	
	@Override
	public void start() throws ApplicationException 
	{
		
	}

	@Override
	public void stop() throws ApplicationException 
	{
		
	}
}
