package org.fenrir.kraft.module;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

import org.fenrir.yggdrasil.cli.commands.CommandMarker;
import org.fenrir.kraft.commands.EmailCommands;
import org.fenrir.kraft.commands.WorkspaceCommands;

public class CommandsModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		Multibinder<CommandMarker> commandBinder = Multibinder.newSetBinder(binder(), CommandMarker.class);
		commandBinder.addBinding().to(WorkspaceCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(EmailCommands.class).in(Singleton.class);
	}
}
