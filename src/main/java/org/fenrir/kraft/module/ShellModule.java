package org.fenrir.kraft.module;

import javax.inject.Singleton;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import org.fenrir.yggdrasil.cli.plugin.BannerProvider;

public class ShellModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		Multibinder<BannerProvider> bannerProviderBinder = Multibinder.newSetBinder(binder(), BannerProvider.class);
//		bannerProviderBinder.addBinding().to(DefaultBannerProvider.class).in(Singleton.class);
	}
}
