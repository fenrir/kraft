package org.fenrir.kraft.commands;

import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.ExitShellRequest;
import org.fenrir.yggdrasil.cli.annotation.CliAvailabilityIndicator;
import org.fenrir.yggdrasil.cli.annotation.CliCommand;
import org.fenrir.yggdrasil.cli.annotation.CliOption;
import org.fenrir.yggdrasil.cli.commands.CommandMarker;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;

public class WorkspaceCommands implements CommandMarker 
{
	private final Logger log = LoggerFactory.getLogger(WorkspaceCommands.class);
	
	@Inject
	private IWorkspaceAdministrationService workspaceService;
	
	public void setWorkspaceService(IWorkspaceAdministrationService workspaceService)
	{
		this.workspaceService = workspaceService;
	}
	
//	@CliAvailabilityIndicator(value={"workspace list"})
//	public boolean isCommandsAvailable()
//	{
//		return workspaceService.getCurrentWorkspaceFolder()!=null;
//	}
	
	@CliCommand(value={"workspace current"}, help="")
	public String currentWorkspace()
	{
		String currentWorkspace = workspaceService.getCurrentWorkspaceFolder();
		if(currentWorkspace==null){
			return "No hi ha cap espai de treball carregat";
		}
		
		return currentWorkspace;
	}
	
	@CliCommand(value={"workspace list"}, help="")
	public void listWorkspaces()
	{	
		try{
			String currentWorkspace = workspaceService.getCurrentWorkspaceFolder();
			List<String> workspaces = workspaceService.getWorkspacesList();
			System.out.println("Espais de treball disponibles: ");
			for(String workspace:workspaces){
				System.out.print("\t" + workspace);
				if(currentWorkspace!=null && currentWorkspace.equals(workspace)){
					System.out.print("*");
				}
				System.out.println("");
			}
		}
		catch(ApplicationException e){
			log.error("Error llistant els espais de treball disponibles: {}", e.getMessage(), e);
			System.err.println("Error llistant els espais de treball disponibles: " + e.getMessage());
		}
	}
	
	@CliCommand(value={"workspace load"}, help="")
	public ExitShellRequest loadWorkspace(@CliOption(key="dir", mandatory=true, help="") String dir)
	{
		try{
            // Es guarden els valors a la configuraci� i es carrega el workspace. Aix� notifica l'event de c�rrega de workspace
            boolean workspaceLoaded = workspaceService.isWorkspaceLoaded();                
            // Si no hi ha workspace carregat es segueix l'execuci� normal
            if(!workspaceLoaded){
                workspaceService.loadWorkspace(dir);
                
                return null;
            }
            // En el cas que hi hagi workspace carregat, es reinicia l'aplicaci�
            else{
                if(log.isDebugEnabled()){
                    log.debug("Especificat reinici en el workspace {}", dir);
                }
                workspaceService.setRestartWorkspace(dir);
                
                System.out.println("Espai de treball carregat");
    			return ExitShellRequest.NORMAL_EXIT;
            }
		}
		catch(ApplicationException|PreferenceException e){
			log.error("Error carregant l'espai de treball: {}", e.getMessage(), e);
			
			System.out.println("Error carregant l'espai de treball: " + e.getMessage());
			return null;
		}
	}
	
	@CliCommand(value={"workspace add"}, help="")
	public String addWorkspace(@CliOption(key="dir", mandatory=true, help="") String dir,
			@CliOption(key="default", mandatory=false, specifiedDefaultValue="true", unspecifiedDefaultValue="false", help="") Boolean isDefault,
			@CliOption(key="load", mandatory=false, specifiedDefaultValue="true", unspecifiedDefaultValue="false", help="") Boolean load)
	{
		try{
			workspaceService.addWorkspace(dir, isDefault);
			if(load){
				loadWorkspace(dir);
			}
			
			return "Espai de treball afegit";
		}
		catch(PreferenceException e){
			log.error("Error afegint l'espai de treball: {}", e.getMessage(), e);
			return "Error afegint l'espai de treball: " + e.getMessage();
		}
	}
}
