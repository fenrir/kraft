package org.fenrir.kraft.commands;

import javax.inject.Inject;

import org.fenrir.yggdrasil.cli.annotation.CliAvailabilityIndicator;
import org.fenrir.yggdrasil.cli.annotation.CliCommand;
import org.fenrir.yggdrasil.cli.annotation.CliOption;
import org.fenrir.yggdrasil.cli.commands.CommandMarker;
import org.fenrir.yggdrasil.core.service.ITaskLauncherService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.kraft.core.task.CheckEmailTask;

public class EmailCommands implements CommandMarker 
{
	@Inject
	private IWorkspaceAdministrationService workspaceService;
	
	@Inject
	private ITaskLauncherService taskLauncherService;
	
	public void setWorkspaceService(IWorkspaceAdministrationService workspaceService)
	{
		this.workspaceService = workspaceService;
	}
	
	public void setTaskLauncherService(ITaskLauncherService taskLauncherService)
	{
		this.taskLauncherService = taskLauncherService;
	}
	
	@CliAvailabilityIndicator(value={"email checkTask"})
	public boolean isCommandsAvailable()
	{
		return workspaceService.getCurrentWorkspaceFolder()!=null;
	}
	
	@CliCommand(value={"email checkTask"}, help="")
	public String startCheckTask(@CliOption(key="start", mandatory=false, specifiedDefaultValue="true", unspecifiedDefaultValue="false", help="") Boolean startOption,
			@CliOption(key="shutdown", mandatory=false, specifiedDefaultValue="true", unspecifiedDefaultValue="false", help="") Boolean shutdownOption)
	{
		if(!startOption && !shutdownOption){
			return "ERROR: S'ha d'especificar una opci� v�lida";
		}
		
		if(startOption){
			taskLauncherService.scheduleTask(CheckEmailTask.ID);
			return "Inicialitzada tasca de comprobaci� de correus";
		}
		else{
			taskLauncherService.shutdownTask(CheckEmailTask.ID);
			return "Aturada tasca de comprobaci� de correus";
		}
	}
}
