define(['backbone', 'marionette'],
function(Backbone, Marionette){
	
	var templateHtml = '<div><button id="myButton" type="submit" id="submit">ProvesN</button></div>';
	
	var Model = Backbone.Model.extend(
	{
		defaults:{
			name: undefined			
		},

		initialize: function(){ }	
	});
	
	var NavigationView = Marionette.ItemView.extend(
	{
		model: new Model('proves'),
		
		template: function(serializedModel){
			var name = serializedModel.name;
			return _.template(templateHtml)({
				name: serializedModel.name
			});
		},

		ui: {
			button: '#myButton'
		},

		events: {
			'click @ui#myButton': 'clickedButton'
		},

		clickedButton: function() {
			console.log('navigationView->clickedButton');
		}
	});
	
	return NavigationView;
});