requirejs.config({
    // Por defecto carga cualquier m�dulo de js/lib
    baseUrl: '/kraft/js/lib',
    /* Los paths son relativos a baseUrl i nunca incluyen ".js" como extension
     */
    paths: {
        jquery: 'jquery-1.7.2.min',
        materialize: 'materialize.min'
    }
});

requirejs(['jquery', 'materialize'],
function($, Materialize)
{
	$('#modal1').openModal({
			dismissible: false, // Modal can be dismissed by clicking outside of the modal
		    opacity: .5, // Opacity of modal background
		    in_duration: 300, // Transition in duration
		    out_duration: 200, // Transition out duration
		    ready: function() { alert('Ready'); }, // Callback for Modal open
		    complete: function() { alert('Closed'); } // Callback for Modal close
	  	}
	);
});
